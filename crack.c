#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "md5.h"
#include "binsearch.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings
int file_length;

int compare(const void *a, const void *b)
{
    return strcmp(*(char**)a, *(char**)b);
}

int bcompare(const void *key, const void *elem)
{
    //printf("bcompare %s with %s\n", *(char **)key, *(char **)elem);
    return strncmp(*(char **)key, *(char **)elem, HASH_LEN -1);
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5

    // Compare the two hashes

    // Free any malloc'd memory

    return 0;
}

// TODO
// Read in the hash file and return the array of strings.
char *read_hashes(char *filename)
{
    // Get size of file
    struct stat info;
    stat(filename, &info);
    file_length = info.st_size;
    
    // Allocate space for the entire file
    char *hashfile = (char *)malloc(file_length+1);
    
    // Read in entire file
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("Can't open file");
        exit(1);
    }
    fread(hashfile, 1, file_length, f);
    fclose(f);
    
    // Add null character to end
    hashfile[file_length] = '\0';
    
    return hashfile;
}

char **parsefile(char *hashfile, int *hashLen)
{
    // Change newlines to nulls, counting as we go
    int lines = 0;
    for (int i = 0; i < file_length; i++)
    {
        if (hashfile[i] == '\n') 
        {
            hashfile[i] = '\0';
            lines++;
        }
    }
    
    if (hashfile[file_length-1] != '\0') lines++;
    
    // Allocate space for an array of pointers
    char **strings = malloc((lines+1) * sizeof(char *));
    
    // Loop through chunk, storing pointers to each string
    int string_idx = 0;
    for (int i = 0; i < file_length; i += strlen(hashfile+i) + 1)
    {
        strings[string_idx] = hashfile+i;
        string_idx++;
    }
    
    // Store terminating NULL
    strings[lines] = NULL;
    *hashLen = lines;
    return strings;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename, int *count)
{
    const int STEPSIZE = 100;
    int arrlen = STEPSIZE;
    
    // Open file
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        perror("Can't open file\n");
        return NULL;
    }
    
    // Allocate space for array of strings
    char **lines = (char **)malloc(arrlen * sizeof(char *));
    
    char buf[PASS_LEN];
    
    int i = 0;
    while(fgets(buf, PASS_LEN, f))
    {
        // Check if array is full, if so extend it.
        if(i == arrlen)
        {
            arrlen += STEPSIZE;
            char **newlines = realloc(lines, arrlen * sizeof(char *));
            if (!newlines)
            {
                perror("Can't reallocate\n");
                exit(1);
            }
            lines = newlines;
        }
        
        int plen = strlen(buf);

        if (plen < 2)
            break;
            
        // Trim off newline
        buf[--plen] = '\0';
        
        //printf( "'%s'", buf);
        
        // Allocate space for pass + hash + :
        char *str = (char *)malloc((HASH_LEN + 1 + plen) * sizeof(char));
        
        // Hash the password
        char *dicthash = md5(buf, plen);
        //printf( " + '%s'", dicthash);
        
        // Copy hash to str
        strcpy(str, dicthash);
        str[HASH_LEN-1] = '/';
        
        // Concat original pass to str
        strcpy(str + HASH_LEN, buf);
        
        // Attach str to data structure
        lines[i] = str;
        i++;
        
        free(dicthash);
        free(str);
    }
    printf("done i = %d\n", i);
    fclose(f);
    *count = i;
    
    
    
    return lines;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char *hashfile = read_hashes(argv[1]);
    int hashLen = 0;
    char **hashes = parsefile(hashfile, &hashLen);

    // TODO: Read the dictionary file into an array of strings
    int dictLen = 0;
    char **dict = read_dict(argv[2], &dictLen);
    
    printf("hash count = %d, pass count %d\n", hashLen, dictLen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dictLen, sizeof(char *), bcompare);
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    char **found;
    
    for( int i = 0; i < hashLen; i++)
    {
        found = bsearch(hashes + i, dict, dictLen, sizeof(char *), bcompare);
        if (found != NULL)
        {
            printf("Found %s\n", *found);
        }
        else
        {
            printf("Could not find hash '%s'\n", hashes[i]);
        }
    }
    
    free(hashfile);
    free(*hashes);
    free(hashes);
    free(*dict);
    free(dict);
}
